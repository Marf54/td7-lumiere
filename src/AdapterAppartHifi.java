import appartement.*;

public class AdapterAppartHifi implements Appareil {

	private AppareilAppartHifi hifi;
	private int son;
	
	public AdapterAppartHifi(AppareilAppart hi) {
		this.hifi = (AppareilAppartHifi)hi;
		this.hifi.changerSon(0);
		this.son = 0;
	}
	
	public void eteindre() {
		if(this.son > 0) {
			this.son--;
			this.hifi.changerSon(this.son);
		}
	}
	
	public void allumer() {
		this.son++;
		this.hifi.changerSon(this.son);
	}
	
	public void inverser() {
		if(this.son > 0)
			this.eteindre();
		else
			this.allumer();
	}
	
	public String toString() {
		return "hifi:" + this.son;
	}
}
