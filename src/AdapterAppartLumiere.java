import appartement.AppareilAppart;
import appartement.AppareilAppartLampe;


public class AdapterAppartLumiere implements Appareil{

	String nom;
	AppareilAppartLampe lampe;
	
	public AdapterAppartLumiere (AppareilAppart l, String n){
		this.lampe = (AppareilAppartLampe)l;
		this.nom = n;
	}
	
	public void allumer (){
		lampe.allumer();
	}
	
	public void eteindre (){
		lampe.eteindre();
	}
	
	public void inverser (){
		if(lampe.toString().indexOf("On") == -1){
			lampe.allumer();
		}else{
			lampe.eteindre();
		}
	}
	
	public String toString (){
		return lampe.toString();
	}

}
