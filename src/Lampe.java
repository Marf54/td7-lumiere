public class Lampe implements Appareil{

	String nom;
	boolean allume;

	
	public Lampe(String paramNom) {
		this.allume = false;
		this.nom = paramNom;
	}

	
	public void allumer() {
		this.allume = true;
	}

	
	public void eteindre() {
		this.allume = false;
	}
	
	public void inverser()
	{
		this.allume = !this.allume;
	}

	
	public String toString()
	{
		String r="";
		if (this.allume)
		{
			r="On";
		}
		else
		{
			r="Off";
		}
		return(nom+": "+r);
	}
}
