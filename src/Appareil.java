/**
 * interface pour abstraire la notion d'appareil
 *  
 * @author thomas8
 *
 */
public interface Appareil {

	/**
	 * permet d'allumer un appreil
	 */	
	public void allumer();

	
	/**
	 * permet d'eteindre un appreil
	 */	
	public void eteindre();
	
	/**
	 * permet d'inverser la commande
	 * ajout � faire ==> c'est l'appareil qui sait g�rer son �tat
	 */
	public void inverser();
	
	
}
