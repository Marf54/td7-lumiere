import java.util.ArrayList;
import java.util.List;

/**
 * classe pour gerer une telecommande universelle
 * @author thomas8
 *
 */
public class Telecommande {

	/**
	 * il s'agit desormais d'une liste d'appareil
	 * -> il faut changer la classe (� la main)
	 * -> il faut changer le nom (refactoring)
	 */
	List<Appareil> appareils;
	
	/**
	 * le construceur bouge un peu
	 */
	public Telecommande()
	{
		appareils=new ArrayList<Appareil>();
	}
	
	/**
	 * la m�thode doit changer puisqu'on ajouter des appareil
	 * changer le nom de la m�thode en refactoring
	 * 	
	 * @param app appareol
	 */
	public void ajouterAppareil(Appareil p)
	{
		appareils.add(p);
	}
	
	/**
	 * renommer la m�thode en activerAppareil
	 * @param i
	 */
	public void activerAppareil(int i)
	{
		appareils.get(i).allumer();	
	}
	
	
	/**
	 * renommer en activerAppareil
	 * @param i
	 */
	public void desactiverAppareil(int i)
	{
		appareils.get(i).eteindre();	
	}
	
	
	/**
	 * ne change pas
	 */
	public void activerTout()
	{
		for (int i=0;i<appareils.size();i++)
			//reutiliser du code existant
			activerAppareil(i);
	}
	
	
	/**
	 * ne change pas mais necessite l'ajout d'une methode inverse
	 * @param i
	 */
	public void appuyer(int i)
	{
		//mauvais emplacement voulu
		//une lampe doit connaitre son iversion
		Appareil lampe = appareils.get(i);
		lampe.inverser();		
	}
	
	
	/**
	 * ne change pas
	 * implique de redefinir tostring dans les classes concretes issues de appareil
	 */
	public String toString()
	{
		String r="";
		for (int i=0;i<appareils.size();i++)
		{
			r=r+appareils.get(i)+"\n";
		}
		return(r);
	}
	
}
