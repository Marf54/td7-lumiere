import appartement.*;

public class AdapterAppartThermostat implements Appareil {

	private AppareilAppartThermostat thermostat;
	private int temperature;
	
	public AdapterAppartThermostat(AppareilAppart therm) {
		this.thermostat = (AppareilAppartThermostat)therm;
		this.temperature = Integer.parseInt(this.thermostat.toString().substring(11));
	}
	
	@Override
	public void allumer() {
		this.thermostat.augmenterTemperature();
		this.temperature++;
	}

	@Override
	public void eteindre() {
		this.thermostat.baisserTemperature();
		this.temperature--;
	}

	@Override
	public void inverser() {
		if(this.temperature > 0) {
			while(this.temperature != -this.temperature) {
				this.thermostat.baisserTemperature();
				this.temperature--;
			}
		} else {
			while(this.temperature != -this.temperature) {
				this.thermostat.augmenterTemperature();
				this.temperature++;
			}
		}
	}

	public String toString() {
		return "thermostat:" + this.temperature;
	}
	
}
