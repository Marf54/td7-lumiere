class Hifi implements Appareil
{

	int son=0;
	
	@Override
	public void allumer() {
		this.son+=10;
		if (this.son>100)
			this.son=100;
		
	}

	@Override
	public void eteindre() {
		this.son=0;
		
	}

	@Override
	public void inverser() {
		if (this.son==0)
			allumer();
		else
			eteindre();
	}
	
	public String toString()
	{
		String r="";
		r+="Hifi:"+son;
		return(r);
	}
	
}